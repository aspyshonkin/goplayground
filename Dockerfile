# This file is a template, and might need editing before it works on your project.
FROM golang:alpine AS builder

WORKDIR /usr/src/app

COPY . .
RUN go-wrapper download
RUN go build -v

FROM golang:alpine
COPY --from=build /usr/src/app /
CMD ["/app"]